let destination = document.getElementById('d1');
let text = '';
const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit"
const vowelsList = ["aa", "ee", "ii", "oo", "uu"]

function createNewDiv(content, styleClass) {
    let newDiv = document.createElement('div');
    newDiv.className = styleClass;
    let textNode = document.createTextNode(content);
    newDiv.appendChild(textNode);
    destination.appendChild(newDiv);
}

//Display an array from the cities in gotCitiesCSV.
createNewDiv('1. Display an array from the cities in gotCitiesCSV.', 'heading')
let gotCitiesArr = gotCitiesCSV.split(',');
createNewDiv(JSON.stringify(gotCitiesArr), '')




//Display an array of words from the sentence in bestThing.
createNewDiv('2. Display an array of words from the sentence in bestThing.', 'heading')
let bestThingArr = bestThing.split(' ');
createNewDiv(JSON.stringify(bestThingArr))



//Display a string separated by semi-colons instead of commas from gotCitiesCSV.
createNewDiv('3. Display a string separated by semi-colons instead of commas from gotCitiesCSV.', 'heading')
createNewDiv(JSON.stringify(gotCitiesCSV.split(',').join(';')));
console.log(typeof gotCitiesCSV.split(',').join(';'))

//Display a CSV (comma-separated) string from the lotrCitiesArray.
createNewDiv('4. Display a CSV (comma-separated) string from the lotrCitiesArray.', 'heading')
createNewDiv(lotrCitiesArray.join(', '))


//Display the first 5 cities in lotrCitiesArray.
createNewDiv('5. Display the first 5 cities in lotrCitiesArray.', 'heading')

for (let i = 0; i < 5; i++) {
    createNewDiv(lotrCitiesArray[i])
}


//Display the last 5 cities in lotrCitiesArray.
createNewDiv('6. Display the last 5 cities in lotrCitiesArray.', 'heading')

const endOfArr = lotrCitiesArray.length - 1
const fiveFromEnd = lotrCitiesArray.length - 5

for (let j = endOfArr; j >= fiveFromEnd; j--) {
    createNewDiv(lotrCitiesArray[j])
}

//Display the 3rd to 5th city in lotrCitiesArray.
createNewDiv('7. Display the 3rd to 5th city in lotrCitiesArray.', 'heading')

for (let i = 3; i < 6; i++) {
    createNewDiv(lotrCitiesArray[i])
}

//Using Array.prototype.splice(), remove "Rohan" from lotrCitiesArray.
createNewDiv('8. Using Array.prototype.splice(), remove "Rohan" from lotrCitiesArray.', 'heading')
createNewDiv(lotrCitiesArray.splice(2, 1))
//Doesn't say to display array. Just says remove... 


//Using Array.prototype.splice(), remove all cities after "Dead Marshes" in lotrCitiesArray.
createNewDiv('9. Using Array.prototype.splice(), remove all cities after "Dead Marshes" in lotrCitiesArray.', 'heading')
lotrCitiesArray.splice(-2, 2)
createNewDiv(lotrCitiesArray)


//Using Array.prototype.splice(), add "Rohan" back to lotrCitiesArray right after "Gondor".
createNewDiv('10. Using Array.prototype.splice(), add "Rohan" back to lotrCitiesArray right after "Gondor".', 'heading')
lotrCitiesArray.splice(2, 0, "Rohan")
createNewDiv(lotrCitiesArray)


//Using Array.prototype.splice(), rename "Dead Marshes" to "Deadest Marshes" in lotrCitiesArray.
createNewDiv('11. Using Array.prototype.splice(), rename "Dead Marshes" to "Deadest Marshes" in lotrCitiesArray.', 'heading')
lotrCitiesArray.splice(5, 1, "Deadest Marshes")
createNewDiv(lotrCitiesArray)


//Using String.prototype.slice(), display the first 14 characters from bestThing.
createNewDiv('12. Using String.prototype.slice(), display the first 14 characters from bestThing.', 'heading')
createNewDiv(bestThing.slice(0, 14))


//Using String.prototype.slice(), display the last 12 characters from bestThing.
createNewDiv('13. Using String.prototype.slice(), display the last 12 characters from bestThing.', 'heading')
createNewDiv(bestThing.slice(-12))


//Using String.prototype.slice(), display characters between the 23rd and 38th position of bestThing (i.e. "boolean is even").
createNewDiv('14. Using String.prototype.slice(), display characters between the 23rd and 38th position of bestThing (i.e. "boolean is even").', 'heading')
createNewDiv(bestThing.slice(22, 38))

//Repeat #13 using String.prototype.substring() instead of String.prototype.slice().
createNewDiv('15. Repeat #13 using String.prototype.substring() instead of String.prototype.slice().', 'heading')
createNewDiv(bestThing.substring(bestThing.length - 12))


//Repeat #14 using String.prototype.substr() instead of String.prototype.slice().
createNewDiv('16. Repeat #14 using String.prototype.substr() instead of String.prototype.slice().', 'heading')
createNewDiv(bestThing.substr(23, 38 - 23))


//Find and display the index of "only" in bestThing.
createNewDiv('17. Find and display the index of "only" in bestThing.', 'heading')
createNewDiv(bestThing.search("only"))



//Find and display the index of the last word in bestThing.
createNewDiv('18. Find and display the index of the last word in bestThing.', 'heading')
createNewDiv(bestThing.search("bit"))


//Find and display all cities from gotCitiesCSV that use double vowels ("aa", "ee", …).
createNewDiv('19. Find and display all cities from gotCitiesCSV that use double vowels ("aa", "ee", …).', 'heading')
let splinterCell = gotCitiesCSV.split(',')
let newArr = []
for (i = 0; i < splinterCell.length; i++) {
    vowelsList.forEach(vowels => { if (splinterCell[i].includes(vowels)) { newArr.push(splinterCell[i]) } })
}
createNewDiv(JSON.stringify(newArr))

//Find and display all cities from lotrCitiesArray that end with "or".

createNewDiv('20. Find and display all cities from lotrCitiesArray that end with "or".', 'heading')

let matrixCity = []
for (i = 0; i < lotrCitiesArray.length; i++) {
    const currentCity = lotrCitiesArray[i]
    if (currentCity.indexOf('or') != -1) {
        matrixCity.push(currentCity)
    }
}
createNewDiv(JSON.stringify(matrixCity))


createNewDiv('21. Find and display all the words in bestThing that start with a "b".', 'heading')

let cell = bestThing.split(' ')
let matrix2 = []
for (i = 0; i < cell.length; i++) {
    const result = cell[i]
    if (result[0] == "b") {
        matrix2.push(result)
    }
}
createNewDiv(JSON.stringify(matrix2))

//Display "Yes" or "No" if lotrCitiesArray includes "Mirkwood".

createNewDiv('22. Display "Yes" or "No" if lotrCitiesArray includes "Mirkwood".', 'heading')
result22 = ''
for (i = 0; i < lotrCitiesArray.length; i++) {
    const cities = lotrCitiesArray[i]
    if (cities == "Mirkwood") {
        result22 = 'Yes'
        break
    } else {
        result22 = 'No'
    }
}
createNewDiv(JSON.stringify(result22))


//Display "Yes" or "No" if lotrCitiesArray includes "Hollywood".

createNewDiv('23. Display "Yes" or "No" if lotrCitiesArray includes "Hollywood".', 'heading')
result23 = ''
for (i = 0; i < lotrCitiesArray.length; i++) {
    const cities = lotrCitiesArray[i]
    if (cities == "Hollywood") {
        result23 = 'Yes'
        break
    } else {
        result23 = 'No'
    }
}
createNewDiv(JSON.stringify(result23))
//let result23b;
// if (lotrCitiesArray.includes('Hollywood')) {
//     result23b = 'Yes'
// } else {
//     result23b = 'No'
// }
// createNewDiv(JSON.stringify(result23b))



//Display the index of "Mirkwood" in lotrCitiesArray.

createNewDiv('24. Display the index of "Mirkwood" in lotrCitiesArray.', 'heading')
const index = lotrCitiesArray.findIndex(city => city === "Mirkwood");
createNewDiv(index)


//Find and display the first city in lotrCitiesArray that has more than one word.

createNewDiv('25. Find and display the first city in lotrCitiesArray that has more than one word.', 'heading')
createNewDiv(lotrCitiesArray[5])


//Reverse the order in lotrCitiesArray.

createNewDiv('26. Reverse the order in lotrCitiesArray.', 'heading')
createNewDiv(lotrCitiesArray.reverse(lotrCitiesArray))


//Sort lotrCitiesArray alphabetically.

createNewDiv('27. Sort lotrCitiesArray alphabetically.', 'heading')
createNewDiv(lotrCitiesArray.sort())


//Sort lotrCitiesArray by the number of characters in each city (i.e., shortest city names go first).

createNewDiv('28. Sort lotrCitiesArray by the number of characters in each city (i.e., shortest city names go first).', 'heading')
createNewDiv(
    lotrCitiesArray.sort(function (a, b) {
        return b.length - a.length;
    }).reverse(lotrCitiesArray))


// Using Array.prototype.pop(), remove the last city from lotrCitiesArray.
createNewDiv('29. Using Array.prototype.pop(), remove the last city from lotrCitiesArray.', 'heading')
lotrCitiesArray.reverse().pop()
createNewDiv(lotrCitiesArray)

//Using Array.prototype.push(), add back the city from lotrCitiesArray that was removed in #29 to the back of the array.

createNewDiv('30. Using Array.prototype.push(), add back the city from lotrCitiesArray that was removed in #29 to the back of the array.', 'heading')
lotrCitiesArray.push("Rohan")
createNewDiv(lotrCitiesArray)


//Using Array.prototype.shift(), remove the first city from lotrCitiesArray.

createNewDiv('31. Using Array.prototype.shift(), remove the first city from lotrCitiesArray.', 'heading')
lotrCitiesArray.shift()
createNewDiv(lotrCitiesArray)


//Using Array.prototype.unshift(), add back the city from lotrCitiesArray that was removed in #31 to the front of the array. 

createNewDiv('32. Using Array.prototype.unshift(), add back the city from lotrCitiesArray that was removed in #31 to the front of the array.', 'heading')
lotrCitiesArray.unshift("Deadest Marshes")
createNewDiv(lotrCitiesArray)